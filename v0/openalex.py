import json

from pyalex import Authors, Works


class WorksCites():

    def __init__(self, openalex_id=None, orcid=None, name=None):
        if openalex_id is not None:
            author_id = openalex_id
        elif orcid is not None:
            author = Authors()[orcid]
            author_id = author['id']
        elif name is not None:
            authors = Authors().search_filter(display_name=name).get()
            author_id = authors[0]['id']
        else:
            raise ValueError("openalex_id, orcid, name, or json needed")

        pager = Works().filter(authorships={"author.id": author_id}) \
            .select(['id', 'display_name', 'publication_date']).paginate()
        self.works = [work for pages in pager for work in pages]
        self.works_cites = {}
        for work in self.works:
            id = work['id']
            if id in self.works_cites:
                print(f"Repeated paper id: {id}")
            else:
                title = work['display_name']
                date = work['publication_date']
                self.works_cites[id] = {'title': title,
                                        'date': date}

    def fill_cites(self):
        for work_id, work_dict in self.works_cites.items():
            cites_pager = Works().filter(cites=work_id) \
                .select(['id', 'display_name', 'publication_date']).paginate()
            cites = [cite for pages in cites_pager for cite in pages]

            cites_dict = {}
            for cite in cites:
                id = cite['id']
                if id in cites_dict:
                    print(f"Repeated cite {id} for paper {work_id}")
                else:
                    title = cite['display_name']
                    date = cite['publication_date']
                    cites_dict[id] = {'title': title,
                                      'date': date}

            work_dict['cites'] = cites_dict

    def cites_per_paper(self):
        papers = []
        for work in self.works_cites.values():
            cites = len(work['cites'])
            papers.append({'title': work['title'], 'cites': cites})
        return papers

    def json_save(self, filename):
        with open(filename, 'w') as file:
            json.dump(self.works_cites, file)
