#!/usr/bin/env python3

# Copyright Jesus M. Gonzalez-Barahona
# SPDX-License-Identifier: GPL-3.0-or-later

"""Produce JSON file for BabiaXR"""

from operator import itemgetter
import json

def new_cite(title, date, cites):
    cite = {'title': title,
            'date': date,
            'area': 1,
            'cites': cites}
    return cite

def fix_title(title):
    """Remove / in title"""
    if title is None:
        return "Unknown title"
    else:
        return title.replace('/', ' ')

def get_date(date):
    return date[:7]

def merge_works(works):
    """Merge papers with the same name, but different ids"""
    work_titles = {}
    merged_works = {}
    for id, work in works.items():
        title = work['title']
        if title in work_titles:
            merge_id = work_titles[title]
            merged_works[merge_id]['date'] = min(merged_works[merge_id]['date'],
                                                 work['date'])
            merged_works[merge_id]['cites'].update(work['cites'])
        else:
            work_titles[title] = id
            merged_works[id] = work
    return merged_works

def work_cites_list(work_dict):
    """Produce a list of dictionaries with title, date, cites for a work"""
    # First, the work as a cite
    if len(work_dict['cites']) == 0:
        return []
    cites_list = []
    title = fix_title(work_dict['title'])
    prev_date = get_date(work_dict['date'])
    cite = new_cite(title=title, date=prev_date, cites=0)
    cites_no = 0
    for cite_dict in sorted(work['cites'].values(), key=itemgetter('date')):
        date = get_date(cite_dict['date'])
        if date != prev_date:
            cites_list.append(cite)
            prev_date = date
        cites_no += 1
        cite = new_cite(title=title, date=date, cites=cites_no)
    cites_list.append(cite)
    for cite in cites_list:
        print(cite)
        print(f"{cite['date']}: {cite['cites']}")
    return cites_list

with open("papers_cites.json", 'r') as file:
    works_cites = json.load(file)

works_cites = merge_works(works_cites)

cites_list = []
for work in works_cites.values():
    cites_list.extend(work_cites_list(work))
cites_list = sorted(cites_list, key=itemgetter('date'))

# Complete, for every date, with all previous papers
complete_list = []
last_list = []
current_cites = {}
current_date = ""

for cite in cites_list:
    if cite['date'] != current_date:
        for title in current_cites:
            work = new_cite(title, current_date, current_cites[title])
            complete_list.append(work)
        current_date = cite['date']
    title = cite['title']
    current_cites[title] = cite['cites']

for title in current_cites:
    work = new_cite(title, current_date, current_cites[title])
    last_list.append(work)

with open("papers_babiaxr.json", 'w') as file:
    json.dump(complete_list, file)

with open("papers_babiaxr_static.json", 'w') as file:
    json.dump(last_list, file, sort_keys=True, indent=2)
