#!/usr/bin/env python3

# Copyright Jesus M. Gonzalez-Barahona
# SPDX-License-Identifier: GPL-3.0-or-later

from operator import itemgetter

from openalex import WorksCites

id = "https://orcid.org/0000-0001-9682-460X"
papers_cites = WorksCites(orcid=id)
papers_cites.fill_cites()
papers = papers_cites.cites_per_paper()
for paper in sorted(papers, key=itemgetter('cites')):
    print(f"{paper['title']}: {paper['cites']}")

papers_cites.json_save('papers_cites.json')