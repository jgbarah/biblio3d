import json
from urllib.parse import parse_qs

from pyscript import document, window, fetch, ffi, when

from openalex import Author as Author_OpenAlex
from dblp import Author as Author_DBLP

supported_classes = {"openalex": Author_OpenAlex,
                     "dblp": Author_DBLP}
supported_databases = list(supported_classes.keys())
supported_identifiers = {database: author_class.identifiers()
                         for database, author_class in supported_classes.items()}

default_ids = {
    "orcid": ["0000-0003-1279-3709"],
    "dblp": ["b/TimBernersLee"],
    "openalex": ["a5025908133"]
}

form = {'database': "", 'identifier': "", 'id': ""}

@when("click", "#database_select")
async def database_handler(event):
    database = event.target.value
    print("Database:", database)
    html_identifiers(supported_identifiers[database], supported_identifiers[database][0])

async def identifier_handler(event):
    identifier = event.target.value
    print("Identifier:", identifier)

@when("click", "#submit_btn")
async def submit_handler(event):
    print("Submit handler")
    database_el = document.querySelector("#database_select")
    identifier_el = document.querySelector("#identifier_select")
    database = database_el.value
    identifier = identifier_el.value
    id_el = document.querySelector("#id_input")
    print(database_el, identifier_el, id_el)
    id = id_el.value
    print(database, identifier, id)
    window.history.pushState(f"{database}, {identifier}, {id}",
                             f"{database}, {identifier}, {id}",
                             f"?db={database}&id={identifier}&value={id}")
    Author = supported_classes[database]
    author = Author(identifier=identifier, id=id)
    await build_all(author)

async def build_city(author):
    print("Author:", author)
    await author.get_works()
    await author.get_citations()
    works = author.citations_static()

    works_json = json.dumps(works)
    querier = document.querySelector("[babia-queryjson]")
    querier_data = f'data: {works_json}'
    print("querier_data:", querier_data)
    print("works:", works)
    for work in works:
        print(work['title'], work['date'])
    querier.setAttribute('babia-queryjson', querier_data)
    window.console.log(querier)

def build_author_summary(author):
    print("Author summary:", author.summary())
    summary_el = document.querySelector("#author_summary")
    summary = author.summary()
    print("Info:", summary)
    summary_el.innerHTML = f"""<h1>Name: {summary['name']}</h1>
            <p>{summary['identifier']}: {summary['id']}</p>
            <p>Works: {summary['works_count']}</p>
            <p>Citations: {summary['citations_count']}</p>
            <p>H-Index: {summary['h_index']}</p>
            """
    window.console.log(summary_el, summary_el.components,summary_el.components.htmlembed)
    summary_el.components.htmlembed.forceRender()

async def build_all(author):
    print("Starting:", author)
    await author.get_author_info()
    await build_city(author)
    build_author_summary(author)

def analyze_qs(qs_str):
    if len(qs_str) == 0:
        database = supported_databases[0]
        identifier = supported_identifiers[database][0]
        value = default_ids[identifier][0]
    else:
        qs = parse_qs(qs_str[1:])
        database = qs.get('db', supported_databases)[0]
        if database not in supported_databases:
            database = supported_databases[0]
        identifier = qs.get('id', supported_identifiers[database])[0]
        if identifier not in supported_identifiers[database]:
            identifier = supported_identifiers[database][0]
        print(identifier, default_ids)
        value = qs.get('value', default_ids[identifier])[0]
    return database, identifier, value

def set_form_values(database, identifier, id):
    database_el = document.querySelector("#database_select")
    database_el.value = database
    print("Database:", database_el.value)
    identifier_el = document.querySelector("#identifier_select")
    identifier_el.value = identifier
    print("Identifier:", identifier_el.value)
    id_el = document.querySelector("#id_input")
    id_el.setAttribute("placeholder", id)
    form = {'database': database, 'identifier': identifier, 'id': id}

def html_databases(databases, selected):
    database_el = document.querySelector("#database_select")
    database_str = ""
    for database in databases:
        option_str = f'<option id="database_{database}" value="{database}">{database}</option>\n'
        database_str += option_str
    database_el.innerHTML = database_str
    database_el.value = selected

def html_identifiers(identifiers, selected):
    print("HTML identifiers:", identifiers, selected)
    identifier_el = document.querySelector("#identifier_select")
    identifier_str = ""
    for identifier in identifiers:
        option_str = f'<option id="identifier_{identifier}" value="{identifier}">{identifier}</option>\n'
        identifier_str += option_str
    identifier_el.innerHTML = identifier_str
    identifier_el.value = selected

async def main():
    database, identifier, id = analyze_qs(window.location.search)
    print("Database, identifier, id:", database, identifier, id)
    html_databases(supported_databases, database)
    html_identifiers(supported_identifiers[database], identifier)
    set_form_values(database=database, identifier=identifier, id=id)
    window.history.pushState(f"{database}, {identifier}, {id}",
                             f"{database}, {identifier}, {id}",
                             f"?db={database}&id={identifier}&value={id}")
    Author = supported_classes[database]
    author = Author(identifier=identifier, id=id)
    await build_all(author)

await main()
