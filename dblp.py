import json
import urllib.parse

from biblio3d import Author as Biblio3D_Author

from pyscript import fetch

# Set the API endpoint and query
endpoint = "https://sparql.dblp.org/sparql"

# List of all papers (DBLP ID)
papers_template_dblp = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?publ ?year ?title WHERE {{
  VALUES ?author {{ <https://dblp.org/pid/{author_id}> }} . 
  ?publ dblp:authoredBy ?author .
  ?publ rdf:type dblp:Publication .
  ?publ dblp:yearOfPublication ?year .
  ?publ dblp:title ?title .
}}
ORDER BY ?year
"""

# List of all papers (ORCID)
papers_template_orcid = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?publ ?year ?title WHERE {{
  ?author dblp:orcid <https://orcid.org/{author_id}> . 
  ?publ dblp:authoredBy ?author .
  ?publ rdf:type dblp:Publication .
  ?publ dblp:yearOfPublication ?year .
  ?publ dblp:title ?title .
}}
ORDER BY ?year
"""

papers_templates = {
    "dblp": papers_template_dblp,
    "orcid": papers_template_orcid}

# List of all papers with citations, those with 0 citations are not included (DBLP ID)
papers_citations_template_dblp = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX cito: <http://purl.org/spar/cito/>
SELECT ?publ ?year ?title ?doi (COUNT(DISTINCT ?cite) as ?citations) WHERE {{
  VALUES ?author {{ <https://dblp.org/pid/{author_id}> }} . 
  ?publ dblp:authoredBy ?author .
  ?publ rdf:type dblp:Publication .
  ?publ dblp:yearOfPublication ?year .
  ?publ dblp:title ?title .
  OPTIONAL {{ ?publ dblp:doi ?doi . }}
  ?publ dblp:omid ?omid .
  ?cite cito:hasCitedEntity ?omid .
}}
GROUP BY ?title ?doi ?year ?publ
ORDER BY ?year
"""

# List of all papers with citations, those with 0 citations are not included (ORCID)
papers_citations_template_orcid = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX cito: <http://purl.org/spar/cito/>
SELECT ?publ ?year ?title ?doi (COUNT(DISTINCT ?cite) as ?citations) WHERE {{
  ?author dblp:orcid <https://orcid.org/{author_id}> . 
  ?publ dblp:authoredBy ?author .
  ?publ rdf:type dblp:Publication .
  ?publ dblp:yearOfPublication ?year .
  ?publ dblp:title ?title .
  OPTIONAL {{ ?publ dblp:doi ?doi . }}
  ?publ dblp:omid ?omid .
  ?cite cito:hasCitedEntity ?omid .
}}
GROUP BY ?title ?doi ?year ?publ
ORDER BY ?year
"""

papers_citations_templates = {
    "dblp": papers_citations_template_dblp,
    "orcid": papers_citations_template_orcid
}

name_template_dblp = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
SELECT ?name WHERE {{
  VALUES ?pers {{ <https://dblp.org/pid/{author_id}> }} .
  ?pers dblp:primaryCreatorName ?name .  
}}
"""

name_template_orcid = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
SELECT ?name WHERE {{
  ?pers dblp:orcid <https://orcid.org/{author_id}> .
  ?pers dblp:primaryCreatorName ?name .  
}}
"""

name_templates = {
    "dblp": name_template_dblp,
    "orcid": name_template_orcid
}

query_template = """
PREFIX dblp: <https://dblp.org/rdf/schema#>
PREFIX cito: <http://purl.org/spar/cito/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <https://schema.org/>
SELECT ?label ?year (?citing as ?dblp) (SAMPLE(?dois) as ?doi) ?url WHERE {{
  VALUES ?publ {{ <{paper_id}> }}
  ?publ dblp:omid ?publ_omid .
  ?cite cito:hasCitedEntity ?publ_omid .
  ?cite cito:hasCitingEntity ?citing_omid .
  OPTIONAL {{
    ?citing dblp:omid ?citing_omid .
    ?citing rdfs:label ?label .
    OPTIONAL {{ ?citing dblp:yearOfPublication ?year . }}
    OPTIONAL {{ ?citing dblp:doi ?dois . }}
  }}
  OPTIONAL {{ ?citing_omid schema:url ?url . }}
}}
GROUP BY ?label ?year ?citing ?url
ORDER BY DESC(?year)
"""

## Not used for now
def get_citations(paper_id: str) -> list[dict]:
    """
    Query the DBLP SPARQL endpoint to retrieve all publications that cite a given paper.

    :param paper_id:
        The paper ID to query for. This should be a valid paper ID in the DBLP database.
    :return: list
        A list of dictionaries containing the paper ID, year, and DOI of each citation.
    """
    headers = {"Accept": "application/sparql-results+json",
               "Content-Type": "application/sparql-query"}

    req = urllib.request.Request(endpoint,
                                 data=query_template.format(paper_id=paper_id).encode(),
                                 headers=headers)
    response = urllib.request.urlopen(req)

    if response.getcode() == 200:
        # Read the response content
        results = response.read()
        results_str = results.decode()
        results_dict = json.loads(results_str)
        return results_dict['results']['bindings']
    else:
        print("Error:", response.getcode())
        return(None)

## Not used for now
def get_citations_year(paper_id: str) -> list[dict]:
    """
    Query the DBLP SPARQL endpoint to retrieve publications per year.

    :param paper_id:
        The paper ID to query for. This should be a valid paper ID in the DBLP database.
    :return: list
        A list of dictionaries containing the year and the number of citations.
    """
    citations = get_citations(paper_id)
    citations_dict = {}
    for citation in citations:
        if (citation is not None) and ('year' in citation):
            year = citation['year']['value']
            citations_dict[year] = citations_dict.get(year, 0) + 1
    citations_list = [(year, citations) for year, citations in citations_dict.items()]
    citations_list = sorted(citations_list, key=lambda x: x[0])
    return citations_list

def compute_hindex(citations):
    citations.sort(reverse=True)
    for i, citation in enumerate(citations):
        if citation < i + 1:
            return i
    return len(citations)

class Author(Biblio3D_Author):
    """Class for managing citations for an author"""

    @classmethod
    def identifiers(cls):
        """Supported author identifiers"""
        return ["orcid", "dblp"]

    async def _sparql_query(self, templates, values):
        """Perform a SPARQL query and return the results

        :param template: template of the query
        :param values: dictionary of values to replace in the template
        :return: results of the query, as a dictionary
        """
        headers = {"Accept": "application/sparql-results+json",
                   "Content-Type": "application/sparql-query"}

        template = templates[self.identifier]
        response = await fetch(endpoint, method='POST',
                               headers=headers,
                               body=template.format(**values))
        if response.status == 200:
            results_dict = await response.json()
            return results_dict['results']['bindings']
        else:
            print("Error:", response.status)
            return None

    async def get_author_info(self):
        """Get author info"""

        result = await self._sparql_query(name_templates,
                                          values={'author_id': self.id})
        self.name = result[0]['name']['value']

    def summary(self):
        print("Citations:", self.works_citations)
        citations = [work['citations'] for work in self.works_citations]
        citations_count = sum(citations)
        h_index = compute_hindex(citations)
        return {
            'name': self.name,
            'identifier': self.identifier,
            'id': self.id,
            'works_count': len(self.works),
            'citations_count': citations_count,
            'h_index': h_index,
        }

    async def get_works(self):
        """Get all works for the author

        Results are stored in self.works.
        """
        works = await self._sparql_query(papers_templates,
                                         values={'author_id': self.id})
        self.works = [{'id': work['publ']['value'],
                      'title': self._fix_title(work['title']['value']),
                      'year': work['year']['value']}
                        for work in works]

    async def get_citations(self):
        """Get list of citations for all works
        """
        works = await self._sparql_query(papers_citations_templates,
                                         values={'author_id': self.id})
        self.works_citations = [{'id': work['publ']['value'],
                             'title': self._fix_title(work['title']['value']),
                             'year': work['year']['value'],
                             'doi': work['doi']['value'],
                             'citations': int(work['citations']['value'])}
                                for work in works]

    def citations_static(self, min_citations=1):

        works = []
        for work in self.works_citations:
            print("Work:", work)
            if work['citations'] >= min_citations:
                data = {
                    'id': work['id'],
                    'title': work['title'],
                    'area': 1,
                    'date': work['year'],
#                    'date': "2024-05-19",
                    'doi': work['doi'],
                    'ccount': work['citations'],
                }
                works.append(data)
        works = sorted(works, key=lambda d: d['date'])
        return works
