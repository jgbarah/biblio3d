
class Author():
    """Class for managing citations for an author"""

    @classmethod
    def identifiers(cls):
        """Supported author identifiers (this root class supports none)"""
        return [""]

    @staticmethod
    def _fix_title(title):
        """Remove / in title"""
        if title is None:
            return "Unknown title"
        else:
            return title.replace('/', ' ')

    def __init__(self, identifier="orcid", id=None):
        """Common instantiation for the Author class tree"""

        if id is None:
            raise ValueError("id needed")
        self.id = id
        if identifier in self.identifiers():
            self.identifier = identifier
        else:
            raise ValueError("Invalid identifier:", identifier)
        self.works = []
