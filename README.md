# Biblio3D

![3D visualization of papers as they get citations](biblio.gif)

3D visualization of citations to papers as columns. Each column corresponds to a paper. Papers in the center are older papers, with newer ones growing around them. The height of each column corresponds to the number of citations to the paper. The static version corresponds to the final count of citations to each paper. In the dynamic version the visualization evolves month by month. showing how papers get citations over time.

[See the demo](https://jgbarah.gitlab.io/biblio3d/cites.html)

Some features:

* Exporting to STL. All visualizations of papers as a city can be exported as a STL file, suitable for being printed in a 3D printer.
* Details of the evolution version. In the evolution version, a controller is provided, to select the direction of the evolution, and to move to specific points in time.
* Virtual reality: look at the visualization in VR in any WebXR-compatible device (Quest, Pico, etc.).
* Desktop: move around using arrow keys, clicking and dragging the mouse.
* Data sources. Visualized data is obtained from [OpenAlex](https://openalex.org), for my ORCID id. However, there is nothing specific in the visualization about this data sources. Other could be used. Merge requests allowing the use of other data sources are welcome.

## How to produce your own visualization

To produce your own visualization:

* Clone this repository with git.

* [Create a Python3 virtual environment, and activate it](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/).

* Install dependencies in `requirements.txt`:

```commandline
pip install -r requirements.txt
```

* In [get_paper_openalex.py](get_paper_openalex.py), change the `id` variable to your ORCID, and run the script. It will produce the [papers_cites.json](papers_cites.json) file for your papers.

* Now, run [produce_babiaxr.py](produce_babiaxr.py), which will produce [papers_babiaxr.json](papers_babiaxr.json) and [papers_babiaxr_static.json](papers_babiaxr_static.json) files for your papers.

* Finally, serve the cloned repository, with these JSON files, via HTTP (with a web server).

* You can now load [citations.html](citations.html) in your browser, as served by the web server.

For example, you can use [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) or [GitHub Pages](https://pages.github.com/) as a server for everything.

Locally, you can also use teh Python3 web server. In the directory with your cloned repository, run:

```commandline
python3 -m http.server
```

This will serve the directory in port 8000. You can now point your browser to http://127.0.0.1:8000/cites.html



## Contributions

If you adapt the scripts to get data from other data sources, you can pull request this repository. I will do my best to integrate them with the rest of the code, if needed.

## License

Copyright 2024 Jesus M. Gonzalez-Barahona

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Credits

Music:

* [Guitar & Saxophone (Instrumental)](https://dig.ccmixter.org/files/tobias_weber/57935) by Aussens@iter
> (c) copyright 2018 Licensed under a Creative Commons Attribution license.  Ft: Stefsax

Software:

* [BabiaXR](https://babiaxr.gitlab.io)
* [A-Frame](https://aframe.io)
