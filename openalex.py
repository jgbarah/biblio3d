from html import unescape

from biblio3d import Author as Biblio3D_Author

from pyscript import document, window, fetch


#from pyalex import Authors, Works

# OpenAlex API end points 
api_eps = {
    'author' : 'https://api.openalex.org/authors/{author}',
}

async def api_get(ep, *args, **kwargs):
    """Get a data structure from the OpenAlex API
    
    Can get it from different kinds of endpoints:
    a string in the api_eps dictionary, a url starting with
    api.openalex, or a url starting with openalex (and in
    that case, it will be changed to api.openalex)

    :param ep: end point
    :param *args: arguments for formatting string (if ep in api_eps)
    :param **kwargs: arguments for formatting string (if ep in api_eps)
    """

    if ep.startswith('https://api.openalex'):
        url = ep
    elif ep.startswith('https://openalex'):
        url = 'https://api.openalex' + ep[16:]
    else:
        url = api_eps[ep].format(*args, **kwargs)
    print("EP:", url)
    resp = await fetch(url)
    if resp.status != 200:
        window.console.log("Status:", resp.status)
        result = None
    else:
        result = await resp.json()
    return result


class Author(Biblio3D_Author):
    """Class for managing citations for an author"""

    @classmethod
    def identifiers(cls):
        """Supported author identifiers"""
        return ["orcid", "openalex"]

    def __init__(self, identifier="orcid", id=None):
        """Instantiation, with OpenAlex id or ORCID id
        
        For authors, we will use its OpenAlex id as their id.
        """

        super().__init__(identifier, id)
        if identifier == "orcid":
            self.author_url = api_eps['author'] \
                .format(author=f'https://orcid.org/{self.id}')
        elif identifier == "openalex":
            self.author_url = api_eps['author'] \
                .format(author=self.id)

    async def get_author_info(self):
        """Get author info"""

        self.info = await api_get(self.author_url)
        print("Info:", self.info)

    def summary(self):
        return {
            'name': self.info['display_name'],
            'identifier': self.identifier,
            'id': self.id,
            'works_count': self.info['works_count'],
            'citations_count': self.info['cited_by_count'],
            'h_index': self.info['summary_stats']['h_index'],
        }

    async def get_works(self):
        """Get all works for the author

        Maximum works per request (page) is 200.
        Thus, we ask pages, with 200 works each, until we have
        all the works (resp['meta']['count']).
        Results are stored in self.works.
        """

        qs_config = 'per-page=200' \
            + '&select=id,title,doi,publication_date,cited_by_count'
        url = f'{self.info["works_api_url"]}&{qs_config}'
        page = 0
        remaining = 1
        while remaining > 0:
            page += 1
            resp = await api_get(f'{url}&page={page}')
            print(resp)
            self.works = self.works + resp['results']
            remaining = resp['meta']['count'] - len(self.works)

    def update_work(self, id, date, ccount, new_id=None):
        """Update id work (in self.works_cites) with data from work
        """

        work = self.works_cites[id]
        work['ccount'] += ccount
        work['date'] = min(work['date'], date)
        if new_id:
            work['other_ids'].append(new_id)

    async def get_citations(self):
        """Get list of works citing author works
        """
        
        # Produce self.work_citations, a dictionary of non-repeated papers
        self.works_cites = {}
        titles = {}
        for work in self.works:
            id = work['id']
            title = work['title']
            print("Title:", title)
            if title is None:
                title = "Unknown title"
            else:
                title = unescape(title)
                title = title.replace('"', "")
                title = title.replace(';', "")
            date = work['publication_date']
            ccount = work['cited_by_count']
            if id in self.works_cites:
                print(f"Repeated paper id: {id}")
                self.update_work(id, date, ccount)
            if title in titles:
                print(f"Repeated paper title: {title}")
                old_id = titles[title]
                self.update_work(id=old_id, date=date, ccount=ccount, new_id=id)
            else:
                doi = work.get('doi', '')
                self.works_cites[id] = {'title': title,
                                        'date': date,
                                        'doi': doi,
                                        'ccount': ccount,
                                        'other_ids': []}
                titles[title] = id

    def citations_static(self, min_citations=1):

        works = []
        for id, work in self.works_cites.items():
            if work['ccount'] >= min_citations:
                data = {
                    'id': id,
                    'title': self._fix_title(work['title']),
                    'area': 1,
                    'date': work['date'],
#                    'date': "2024-05-19",
                    'doi': work['doi'],
                    'ccount': work['ccount'],
                }
                works.append(data)
        works = sorted(works, key=lambda d: d['date'])
        return works

    def fill_cites(self):
        for work_id, work_dict in self.works_cites.items():
            cites_pager = Works().filter(cites=work_id) \
                .select(['id', 'display_name', 'publication_date']).paginate()
            cites = [cite for pages in cites_pager for cite in pages]

            cites_dict = {}
            for cite in cites:
                id = cite['id']
                if id in cites_dict:
                    print(f"Repeated cite {id} for paper {work_id}")
                else:
                    title = cite['display_name']
                    date = cite['publication_date']
                    cites_dict[id] = {'title': title,
                                      'date': date}

            work_dict['cites'] = cites_dict

    def cites_per_paper(self):
        papers = []
        for work in self.works_cites.values():
            cites = len(work['cites'])
            papers.append({'title': work['title'], 'cites': cites})
        return papers

    def json_save(self, filename):
        with open(filename, 'w') as file:
            json.dump(self.works_cites, file)
